flaskapp.com - Flask and gunicorn
=================================

Basic Flask app template set up to use masterless salt for development and deployment

Deployment with masterless-salt
===============================

To use this you will need Vagrant 1.7.2 or higher as well as LXC and the vagrant-lxc plugin for it.

You may also get a message about needing to install the `redir` package::

$ sudo apt-get install redir

(you can modify the VagrantFile to use a different provider if you do not have access to linux containers)

The template can be used as-is, the default app name is "flaskapp" but that can changed in the
pillar data under `salt/pillar/base/app.sls`

Development
-----------
Clone the repository then from inside the root folder run::

$ vagrant up --provider=lxc

This command will start up a vagrant instance and set the server running, you can then access it
from your host machine by navigating to http://localhost:8080

Production
----------
Install salt-call::

$ wget -O - https://bootstrap.saltstack.com | sudo sh

Clone the repository into `/home/<user>/` or elsewhere (you can devise your own versioning
system) then symlink the folder contents into `/srv/www/` and run::

$ salt-call --config-dir=/srv/www/salt/ pillar='{"password":"passwdhash"}' state.highstate saltenv=prod

The pillar argument will be a serialised dictionary of any sensitive pillar information that you do not wish to
commit to the repository and only want to provide at runtime e.g. password hashes.

Make a password hash for the user using::

$ mkpasswd -m sha-256