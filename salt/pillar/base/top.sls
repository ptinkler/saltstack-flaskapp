dev:
  '*':
    - app
    - source
    - environment
    - requirements
    - user
    - virtualenv
    - gunicorn

prod:
  '*':
    - app
    - source
    - environment
    - requirements
    - user
    - virtualenv
    - gunicorn
