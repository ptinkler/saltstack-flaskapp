{{ pillar['source_dir'] }}/src/config.py:
  file.managed:
    - source: salt://app/config-example.py.txt
    - template: jinja
    - user: {{ pillar['user'] }}
    - group: {{ pillar['user'] }}