include:
  - supervisord

nginx:
  pkg:
    - installed
  service.running:
    - enable: True
    - restart: True
    - watch:
      - file: /etc/nginx/sites-enabled/{{ pillar['app_name'] }}.conf
      - file: /etc/nginx/sites-enabled/default
      - pkg: nginx

# Remove default nginx site
/etc/nginx/sites-enabled/default:
  file.absent: []

/etc/nginx/sites-enabled:
  file.directory:
    - require:
        - pkg: nginx

/etc/nginx/sites-enabled/{{ pillar['app_name'] }}.conf:
  file.symlink:
    - target: /etc/nginx/sites-available/{{ pillar['app_name'] }}.conf
    - watch:
        - file: /etc/nginx/sites-enabled
        - file: /etc/nginx/sites-available/{{ pillar['app_name'] }}.conf

/etc/nginx/sites-available/{{ pillar['app_name'] }}.conf:
  file.managed:
    - source: salt://nginx/nginx-app.conf
    - template: jinja
    - require:
      - pkg: nginx