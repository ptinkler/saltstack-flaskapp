{{ pillar['app_name'] }}-virtualenv:
  virtualenv.managed:
    - name: {{ pillar['virtualenv_dir'] }}/{{ pillar['app_name'] }}
    - no_site_packages: True
    - cwd: {{ pillar['virtualenv_dir'] }}/{{ pillar['app_name'] }}
    - user: {{ pillar['user'] }}
    - require:
      - user: {{ pillar['user'] }}
      - pkg: python-pkgs

{{ pillar['app_name'] }}-reqs-file:
  file.exists:
    - name: {{ pillar['requirements_dir'] }}/{{ pillar['env'] }}.txt

{{ pillar['app_name'] }}-reqs:
  pip.installed:
    - pip_exists_action: switch
    - requirements: {{ pillar['requirements_dir'] }}/{{ pillar['env'] }}.txt
    - cwd: {{ pillar['virtualenv_dir'] }}/{{ pillar['app_name'] }}
    - pip_bin: {{ pillar['virtualenv_dir'] }}/{{ pillar['app_name'] }}/bin/pip
    - bin_env: {{ pillar['virtualenv_dir'] }}/{{ pillar['app_name'] }}
    - requires:
      - file: {{ pillar['source_dir'] }}
      - file: {{ pillar['app_name'] }}-reqs-file
