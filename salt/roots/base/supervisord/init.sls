/etc/supervisor/supervisord.conf:
  file.managed:
    - source: salt://supervisord/supervisord.conf
    - makedirs: True
    - template: jinja

/etc/supervisor/conf.d/{{ pillar['app_name'] }}.conf:
  file.managed:
    - source: salt://supervisord/supervisor-app.conf
    - makedirs: True
    - template: jinja

supervisor:
  pkg:
    - installed