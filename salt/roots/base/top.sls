dev:
  '*':
    - user
    - app
    - python
    - supervisord
    - nginx
    - gunicorn

prod:
  '*':
    - user
    - app
    - python
    - supervisord
    - nginx
    - gunicorn