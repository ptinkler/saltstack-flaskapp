import logging

from flask import Flask

from werkzeug.contrib.fixers import ProxyFix


app = Flask(__name__)
app.config.from_object('src.config')
app.wsgi_app = ProxyFix(app.wsgi_app)


@app.before_first_request
def setup_logging():
    if not app.config['DEBUG']:
        # In production mode, add log handler to sys.stderr
        # app is internal so we want debug level logging
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.DEBUG)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)

from src.app import views